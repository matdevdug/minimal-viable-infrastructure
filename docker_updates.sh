#!/bin/bash

# Get the directory of the docker-compose file (replace "docker-compose.yml" with your filename)
PROJECT_DIR=$(dirname "$(realpath "$1")")
COMPOSE_FILE="$PROJECT_DIR/docker-compose.yml"

# Exit if the file is not found
if [ ! -f "$COMPOSE_FILE" ]; then
  echo "Error: Docker Compose file '$COMPOSE_FILE' not found!"
  exit 1
fi

# Function to check for image update
check_update() {
  local service_name="$1"
  local image_name=$(docker-compose config --services "$service_name" | grep image: | awk '{print $2}')

  # Extract image name without tag (if any)
  local image=$(echo "$image_name" | cut -d ":" -f 1)

  # Check for latest tag
  local latest_image=$(docker images "$image" --filter "reference=latest" -q | head -n 1)
  local current_image=$(docker ps -a --filter "name=^$service_name" -q | xargs docker inspect --format '{{.Config.Image}}')

  # Compare image digests
  if [ "$latest_image" != "$current_image" ]; then
    echo "Update available for $service_name ($image:latest)"
    return 0  # Update available
  fi

  return 1  # No update
}

# Loop through services, check for updates, and restart if needed
for service in $(docker-compose config --services); do
  if check_update "$service"; then
    docker-compose up -d "$service"
    echo "Restarted service: $service"
  fi
done
