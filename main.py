import tomli
from dotenv import load_dotenv
import os
import sys
from hetzner import *
# Load the config file and the secret environment variables

with open("config.toml", "rb") as config_file:
    config = tomli.load(config_file)

# Load the secret environment variables
load_dotenv()

# Create a client with the help of the config file
public_key: str = os.getenv("PUBLIC_KEY")
api_key: str = os.getenv("HETZNER_API_KEY")
client = create_hetzner_client(api_key)

# Read the cloud-init file
cloud_init: str = read_cloud_init_file_hetzner("cloud-init.yaml")

# Define the architecture
arch = config["hetzner"]["architecture"]

# Check Network
if check_if_network_exists_hetzner(client, config["network"]["name_of_network"]) == True:
    print(f"Network {config['network']['name_of_network']} already exists")
else:
    create_network_hetzner(client, config["network"]["name_of_network"], config["network"]["ip_range"])
    print(f"Network {config['network']['name_of_network']} created")

# Check Subnet
if check_if_subnet_exists_hetzner(client, config["network"]["name_of_network"], config["network"]["subnet_range"]) == True:
    print(f"Subnet {config['network']['subnet_range']} already exists")
else:
    create_subnet_hetzner(client, config["network"]["name_of_network"], config["network"]["subnet_range"])
    print(f"Subnet {config['network']['subnet_range']} created")

# Check SSH Key
if check_if_ssh_key_exists_hetzner(client, config["ssh_key"]["name"]) == True:
    print(f"SSH key {config['ssh_key']['name']} already exists")
else:
    create_ssh_key_hetzner(client, config["ssh_key"]["name"], public_key)
    print(f"SSH key {config['ssh_key']['name']} created")

# Check Servers
current_servers = check_if_servers_match_config_hetzner(client, config["servers"]["name_of_servers"])
print(current_servers)

if current_servers == True:
    print("We have enough servers")
else:
    server_type = config["hetzner"]["type"]
    image = get_latest_debian_image_hetzner(client, arch)
    ssh_key = config["ssh_key"]["name"]
    network_name = config["network"]["name_of_network"]
    create_volume = config["servers"]["create_volume"]
    ipv6 = config["servers"]["ipv6"]
    ipv4 = config["servers"]["ipv4"]
    if create_volume == True:
        volume_size = config["servers"]["volume_size"]
    else:
        volume_size = None

    create_server_hetzner(client, current_servers, server_type, image, ssh_key, network_name, create_volume, volume_size, cloud_init, ipv6, ipv4)

# Write out server information
all_servers = get_server_ips_hetzner(client)
write_server_ips_to_file_hetzner(all_servers)


# Check Load Balancer
