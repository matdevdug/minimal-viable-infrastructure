# Minimal Viable Infrastructure

### Purpose
Come up with a simple CLI that allows small orgs and single developer companies to spin up and maintain what would be considered minimally viable infrastructure

### Motivation
I keep talking to small startups and small businesses who feel setting up the basic server stuff is too complicated. I wanted to write something with the goal of getting you to a size where it makes sense to bring in someone better equipped to deal with stuff like servers. So the goal here is for small business to have a one-run tool. No more than 1 load balancer, no more than 25 VMs, keep it relatively simple.

### Design Roadmap
1. Write the basic framework in Python, so I can figure out what is required. First targets will be Hetzner + Cloudflare. Then Digital Ocean and maybe Linode. 
2. Once I have the structure of what is required, figure out the plugin strategy so that adding different cloud providers doesn't require too much additional knowledge. 
3. When the CLI is in good shape, rewrite it in Golang so that it is easier to compile and deploy. I'm not starting with Golang because I personally work much faster in Python for testing. Feel free to do things however you want. However the goal of a v1 is a fully viable python CLI. The goal of a v2 would be to dump the python for Golang. 

### Proposed Workflow
The idea here is to set up a totally hands-free infrastructure design. So you would push to Docker Hub, your servers would be configured to poll Docker Hub and pull down the images and deploy them to your servers. cloud-init would handle the initial setup of the servers on the cloud provider side.

### Why not ansible/terraform/pulumi/etc
If you are interested in using those technologies, this tool is too basic for you. The idea here is something where a person entirely uninterested in setting up infrastructure would be able to get things running on a lower-cost cloud provider without needing to engage with or talking to any experts. 

