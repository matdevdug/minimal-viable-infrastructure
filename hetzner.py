from hcloud import Client
from hcloud.images import Image
from hcloud.server_types import ServerType
from hcloud.servers import ServerCreatePublicNetwork
from hcloud.ssh_keys import SSHKey
from hcloud.networks.domain import NetworkSubnet
import os
import logging
import json
from time import sleep

logging.basicConfig(level=logging.INFO, filename='infra.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

def create_hetzner_client(api_key):
    try:
        client = Client(token=api_key)
        return client
    except:
        print("API key is invalid. Please check and try again.")
        logging.error("API key is invalid. Please check and try again.")
        return None

def get_latest_debian_image_hetzner(client, arch):
    image_id_list = []
    images: str = client.images.get_all(architecture=arch)
    for image in images:
        if "debian" in image.name.lower():
            image_id_list.append(image.id)
            return(max(image_id_list))
            logging.info(f"Debian image found with ID {max(image_id_list)}.")
        else:
            print("No Debian image found. Check your API key and try again.")
            logging.error("No Debian image found. Check your API key and try again.")
            return None

def check_if_network_exists_hetzner(client, network_name):
    networks = client.networks.get_all()
    if len(networks) > 0:
        for network in networks:
            if network.name == network_name:
                logging.info(f"Network {network_name} exists.")
                return True
            else:
                return False
                logging.info(f"Network {network_name} does not exist.")

def check_if_load_balancer_exists_hetzner(client, lb_config, lb_name):
    if lb_config == True:
        load_balancers = client.load_balancers.get_by_name(lb_name)
        if load_balancers.name.lower() == lb_name.lower():
            return True
        else:
            return False


def create_network_hetzner(client, network_name, ip_range):
    network = client.networks.create(name=network_name, ip_range=ip_range)
    return network

def check_if_subnet_exists_hetzner(client, network_name, ip_range):
    vpc = client.networks.get_by_name(network_name)
    subnets = vpc.subnets
    if len(subnets) > 0:
        for subnet in subnets:
            logging.info(f"Subnet {subnet.ip_range} exists.")
            if subnet.ip_range == ip_range:
                logging.info(f"Subnet {subnet.ip_range} matches config file.")
                return True
            else:
                return False
                logging.info(f"Subnet {subnet.ip_range} does not match config file.")

def create_subnet_hetzner(client, network_name, config_ip_range):
    vpc = client.networks.get_by_name(network_name)
    subnets = vpc.subnets
    new_subnet = NetworkSubnet(type="cloud", network_zone="eu-central", ip_range=config_ip_range)
    vpc.create_subnet(new_subnet)
    return new_subnet

def check_if_ssh_key_exists_hetzner(client, key_name):
    existing_keys = client.ssh_keys.get_by_name(key_name)
    if existing_keys.name.lower() == key_name.lower():
        return True
    else:
        return False

def create_ssh_key_hetzner(client, key_name, public_key):
    key = client.ssh_keys.create(name=key_name, public_key=public_key)
    print(f"SSH key {key_name} created with ID {key.id}.")
    return key

def check_if_servers_match_config_hetzner(client, desired_servers):
    existing_servers = []
    servers = client.servers.get_all(status="running")
    for server in servers:
        existing_servers.append(server.name)
        logging.info(f"Server {server.name} found.")

    a = set(existing_servers)
    b = set(desired_servers)

    if a == b:
        return True
    else:
        servers_to_create = (b - a)
        return servers_to_create

def create_server_hetzner(client, servers_to_create, server_type, image_id, ssh_key, network_name, create_volume, volume_size, cloud_init, ipv6_enabled, ipv4_enabled):
    vpc = client.networks.get_by_name(network_name)
    subnet = vpc.subnets[0]
    ssh_keys = client.ssh_keys.get_by_name(ssh_key)

    for server_name in servers_to_create:
        new_server = client.servers.create(
            name = server_name,
            server_type = ServerType(name=server_type),
            image = Image(id=image_id),
            ssh_keys = [ssh_keys],
            networks = [vpc],
            labels = {
                "replace": "false",
                "os": "debian",
                "type": "worker"
                },
            user_data = cloud_init,
            public_net = ServerCreatePublicNetwork(
                enable_ipv6 = ipv6_enabled,
                enable_ipv4 = ipv4_enabled,
                )
            )
        print(f"Server {server_name} created.")
        # Give the server time to boot before creating a volume
        sleep(5)
        print(f"Server ID: {new_server.server.id}")

        if create_volume == True:
            new_volume = client.volumes.create(
                    name = f"{server_name}-volume",
                    size = volume_size,
                    server = new_server.server,
                    automount = True,
                    format = "ext4"
                    )
            print(f"Volume {new_volume.volume.name} created and attached to {server_name}.")

# Return both the public and private IP address of every server in the account sorted by server name and write to a json file
def get_server_ips_hetzner(client):
    servers = client.servers.get_all()
    server_list = []
    for server in servers:
        if server.status == "running" and len(server.private_net) != 0:
            if server.public_net.ipv6 is None and server.public_net.ipv4 is not None:
                server_list.append({"name": server.name, "public_ip_v4": server.public_net.ipv4.ip, "public_ip_v6": "None", "private_ip": server.private_net[0].ip})
            elif server.public_net.ipv4 is None and server.public_net.ipv6 is not None:
                server_list.append({"name": server.name, "public_ip_v4": "None", "public_ip_v6": server.public_net.ipv6.ip, "private_ip": server.private_net[0].ip})
            elif server.public_net.ipv4 is not None and server.public_net.ipv6 is not None:
                server_list.append({"name": server.name, "public_ip_v4": server.public_net.ipv4.ip, "public_ip_v6": server.public_net.ipv6.ip, "private_ip": server.private_net[0].ip})
        elif server.status == "running" and len(server.private_net) == 0:
            if server.public_net.ipv6 is None and server.public_net.ipv4 is not None:
                server_list.append({"name": server.name, "public_ip_v4": server.public_net.ipv4.ip, "public_ip_v6": "None", "private_ip": "None"})
            elif server.public_net.ipv4 is None and server.public_net.ipv6 is not None:
                server_list.append({"name": server.name, "public_ip_v4": "None", "public_ip_v6": server.public_net.ipv6.ip, "private_ip": "None"})
            elif server.public_net.ipv4 is not None and server.public_net.ipv6 is not None:
                server_list.append({"name": server.name, "public_ip_v4": server.public_net.ipv4.ip, "public_ip_v6": server.public_net.ipv6.ip, "private_ip": "None"})
    return server_list

def write_server_ips_to_file_hetzner(server_list):
    with open("server_ips.json", "w") as f:
        for server in server_list:
            json.dump(server, f, indent=4)

def read_cloud_init_file_hetzner(cloud_init_file):
    try:
        with open(cloud_init_file, "r") as f:
            lines = f.readlines()
            for i in range(len(lines)):
                lines[i] = os.path.expandvars(lines[i])
            return '\n'.join(lines)
    except FileNotFoundError:
        print(f"File {cloud_init_file} not found.")
        return None

